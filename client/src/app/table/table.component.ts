import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {MatTabChangeEvent} from '@angular/material';
import {SnackBarServiceService} from '../services/snack-bar-service.service';
import {UserModel} from '../models/user-model';
import {Title} from "@angular/platform-browser";

export interface ColumnsModel {
  position: number;
  login: string;
  score: number;
  games: number;
}

@Component({
  selector: 'webapp-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnInit {

  displayedColumns: string[];
  tableData: ColumnsModel[];
  withWalls: boolean;

  constructor(
    private userService: UserServiceService,
    private snackBar: SnackBarServiceService,
    private titleService: Title
  ) {

    this.displayedColumns = ['position', 'login', 'score', 'games'];
    this.tableData = [];
    this.withWalls = false;
    this.getTopPlayers(this.withWalls);
  }

  ngOnInit() {
    this.titleService.setTitle('ТОП 20 игроков');
  }

  switchTabEvent(event: MatTabChangeEvent) {
    this.withWalls = !this.withWalls;
    this.getTopPlayers(this.withWalls);
  }

  getTopPlayers(withWalls: boolean) {
    this.userService.getTopPlayers(this.withWalls).subscribe(
      (userList: UserModel[]) => {
        this.tableData = [];
        if (withWalls) {
          for (let i = 0; i < userList.length; i++) {
            this.tableData.push(
              {
                position: i + 1,
                login: userList[i].login,
                score: userList[i].globalscorewithwalls,
                games: userList[i].gameswithwalls
              }
            );
          }
        } else {
          for (let i = 0; i < userList.length; i++) {
            this.tableData.push(
              {
                position: i + 1,
                login: userList[i].login,
                score: userList[i].globalscorenowalls,
                games: userList[i].gamesnowalls
              }
            );
          }
        }

      },
      () => {
        this.snackBar.open('Не удалось загрузить таблицу');
      }
    );
  }

}
