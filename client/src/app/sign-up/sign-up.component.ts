import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {SnackBarServiceService} from '../services/snack-bar-service.service';
import {UserModel} from '../models/user-model';
import {UserServiceService} from '../services/user-service.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'webapp-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.less']
})
export class SignUpComponent implements OnInit {

  formGroup: FormGroup;

  isClicked: boolean;

  constructor(
    private userService: UserServiceService,
    private router: Router,
    private snackBar: SnackBarServiceService,
    private titleService: Title
  ) {

    this.isClicked = false;

    this.formGroup = new FormGroup({
        loginF: new FormControl(''),
        passwordF: new FormControl(''),
        repeatPasswordF: new FormControl('')
      }
    );
  }

  ngOnInit() {
    this.titleService.setTitle('Создание аккаунта');
  }

  getLoginErrorMessage() {
    return this.formGroup.get('loginF').hasError('pattern') ? 'Это имя уже занято' :
      '';
  }

  getRepeatPasswordErrorMessage() {
    return this.formGroup.get('repeatPasswordF').hasError('pattern') ? 'Пароли не совпадают' :
      '';
  }

  doPasswordsMatch() {
    return this.formGroup.get('repeatPasswordF').value === this.formGroup.get('passwordF').value;
  }

  submit() {
    if (!this.doPasswordsMatch()) {
      this.formGroup.get('repeatPasswordF').setErrors({pattern: true});
    }
    if (!this.isClicked && this.formGroup.valid) {
      this.isClicked = true;
      this.signUp();
    }

  }

  signUp() {
    const theUser = new UserModel(
      this.formGroup.get('loginF').value.toString().replace(/\s/g, ''),
      this.formGroup.get('passwordF').value.toString().replace(/\s/g, ''),
      null,
      0,
      0.0,
      0,
      0.0
    );

    this.userService.signUp(theUser).subscribe(
      () => {
        this.snackBar.open('Ваш новый аккаунт был успешно создан');
        this.router.navigateByUrl('/sign-in');
        this.isClicked = false;
      },
      response => {
        if (response.error === 'LoginBusy') {
          this.formGroup.get('loginF').setErrors({pattern: true});
        } else {
          this.snackBar.open('Зарегистрироваться не удалось. Попробуйте ещё раз');
        }

        this.isClicked = false;
      }
    );

  }


}
