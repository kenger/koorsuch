import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {StorageService} from './storage.service';
import {HttpServiceService} from './http-service.service';
import {UserModel} from '../models/user-model';
import {map} from 'rxjs/operators';
import {SnackBarServiceService} from './snack-bar-service.service';
import {ProgressModel} from '../models/progress-model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class UserServiceService {

  private user: UserModel;
  private amIplaying: boolean;

  // локальные очки
  globalScoreNoWalls: number;
  globalScoreWithWalls: number;

  // наличие сохраненной игры
  doIhaveSavedGame: boolean;

  constructor(
    private storage: StorageService,
    private http: HttpServiceService,
    private snackBar: SnackBarServiceService,
    private router: Router) {

    this.amIplaying = false;
  }

  // Source: https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#Solution_4_%E2%80%93_escaping_the_string_before_encoding_it
  private b64EncodeUnicode(login: string, password: string) {
    return btoa(encodeURIComponent(login + ':' + password).replace(/%([0-9A-F]{2})/g,
      function toSolidBytes(match, p1) {
        return String.fromCharCode((Number)('0x' + p1));
      })
    )
  }

  getAmIplaying() {
    return this.amIplaying;
  }

  setAmIplaying(value: boolean) {
    this.amIplaying = value;
  }

  signUp(user: UserModel) {
    return this.http.put('/users/signUp', null, user);
  }

  signIn(login: string, password: string) {
    const token = 'Basic ' + this.b64EncodeUnicode(login, password);
    const headers = new HttpHeaders({Authorization: token});

    return this.http.get('/users/signIn', headers).pipe(
      map(
        userData => {
          this.storage.write('token', token);
          this.storage.write('login', login);
          return userData;
        }
      )
    );
  }

  getUser() {
    return this.http.get('/users/getUser', null, this.user);
  }

  signOut() {
    this.storage.delete('login');
    this.storage.delete('token');
    this.doIhaveSavedGame = false;
    this.globalScoreNoWalls = null;
    this.globalScoreWithWalls = null;
    this.router.navigateByUrl('/');
    this.snackBar.open('Выход из аккаунта был успешно выполнен');
  }

  isSignedIn() {
    return this.storage.contains('token');
  }

  getLogin() {
    return this.storage.read('login');
  }

  getUserGameInfo() {
    if (this.isSignedIn()) {
      this.getUser().subscribe(
        (myUser: UserModel) => {
          this.globalScoreNoWalls = myUser.globalscorenowalls;
          this.globalScoreWithWalls = myUser.globalscorewithwalls;
          if (myUser.progress) {
            this.doIhaveSavedGame = true;
          }
        },
        () => {
          this.signOut();
          this.snackBar.open('Не могу загрузить данные о Вашем аккаунте...');
        }
      );
    }
  }

  getBoardInfo() {
    return this.http.getBoardInfo();
  }

  finish(win: boolean, gameMode: string, score: number) {
    return this.http.get('/users/finish/' + win + '/' + gameMode + '/' + score, null).subscribe(
      (globalScore: number) => {
        this.doIhaveSavedGame = false;
        this.setGlobalScoreScore(globalScore, gameMode);
        if (win) {
          this.snackBar.open('ПОЗДРАВЛЯЕМ!!! Вы выиграли!');
        } else {
          this.snackBar.open('Вы проиграли :(');
        }
        this.setAmIplaying(false);
      },
      () => {
        this.snackBar.open('Нет соединения с сервером. Ваш результат игры не был сохранён');
      }
    );
  }

  updateProgress(progress: ProgressModel) {
    return this.http.put('/users/updateProgress', null, progress);
  }

  getProgress() {
    return this.http.get('/users/getProgress', null);
  }

  setGlobalScoreScore(value: number, gameMode: string) {
    if (gameMode.startsWith('No Walls')) {
      this.globalScoreNoWalls = value;
    } else if (gameMode.startsWith('With Walls')) {
      this.globalScoreWithWalls = value;
    }
  }

  getTopPlayers(withWalls: boolean) {
    return this.http.get('/users/top/' + withWalls, null);
  }

  deleteAccount(currentPassword: string) {
    return this.http.deleteAcc('/users/deleteAcc/' + currentPassword);
  }

  changePassword(currentPass: string, newPass: string) {
    return this.http.get(
      '/users/changePassword?currentPassword=' + currentPass + '&newPassword=' + newPass,
      null
    );
  }

  updatePasswordInToken(password: string) {
    const token = 'Basic ' + this.b64EncodeUnicode(
      this.getLogin(),
      password
    );
    this.storage.write('token', token);
  }
}
