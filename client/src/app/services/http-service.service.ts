import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CellModel} from '../models/cell-model';
import {Observable} from 'rxjs';
import {StorageService} from './storage.service';
import {environment} from '../../environments/environment';
import {combineAll} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(
    private http: HttpClient,
    private storage: StorageService) {
  }

  // HEADERS
  private getHeaders(): HttpHeaders {
    return new HttpHeaders({Authorization: this.storage.read('token')});
  }

  // GET
  public get(url: string, addedHeaders?: HttpHeaders, object?: any): Observable<typeof object> {
    if (addedHeaders) {
      return this.http.get<typeof object>(environment.url + url, {headers: addedHeaders});
    }
    if (this.storage.contains('token')) {
      return this.http.get<typeof object>(environment.url + url, {headers: this.getHeaders()});
    }
    return this.http.get<typeof object>(environment.url + url);
  }

  // PUT
  public put(url: string, addedHeaders?: HttpHeaders, object: any = null): Observable<typeof object> {
    if (addedHeaders) {
      return this.http.put<typeof object>(environment.url + url, object, {headers: addedHeaders});
    }
    if (this.storage.contains('token')) {
      return this.http.put<typeof object>(environment.url + url, object, {headers: this.getHeaders()});
    }
    return this.http.put<typeof object>(environment.url + url, object);
  }

  // DELETE Account
  public deleteAcc(url: string) {
    return this.http.delete(environment.url + url, {headers: this.getHeaders()});
  }

  // FRUIT
  public genFruitCoordinates(snakeParts: []) {
    return this.http.put<CellModel>(environment.url + '/game/generateFruit', snakeParts);
  }

  // BOARD INFO
  public getBoardInfo() {
    return this.http.get(environment.url + '/game/info');
  }
}
