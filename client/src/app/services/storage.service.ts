import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  public write(key: string, item: string) {
    localStorage.setItem(key, item);
  }

  public read(key: string) {
    const item = localStorage.getItem(key);
    if (!item) {
      throw new Error(`item ${key} is not found.`);
    }
    return item;
  }

  public delete(key: string) {
    localStorage.removeItem(key);
  }

  public contains(key: string): boolean {
    const item = localStorage.getItem(key);
    return !!item;
  }

}
