import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackBarServiceService {

  constructor(private snackBar: MatSnackBar) { }

  open(message: string) {
    this.snackBar.open(message, 'Закрыть', {
      duration: 3000
    });
  }

  openAndTimeIs(message: string, mSeconds: number) {
    this.snackBar.open(message, '', {
      duration: mSeconds
    });
  }

}
