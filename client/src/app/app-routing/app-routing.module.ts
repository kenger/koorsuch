import { NgModule } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SignUpComponent} from '../sign-up/sign-up.component';
import {SignInComponent} from '../sign-in/sign-in.component';
import {GameComponent} from '../game/game.component';
import {ProfileComponent} from '../profile/profile.component';
import {TableComponent} from '../table/table.component';

const routes: Routes = [
  {path: 'sign-up', component: SignUpComponent},
  {path: 'sign-in', component: SignInComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'table', component: TableComponent},
  {path: '**', component: GameComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [{provide: APP_BASE_HREF, useValue: '/'}]
})
export class AppRoutingModule { }
