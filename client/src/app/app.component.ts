import {Component} from '@angular/core';
import {UserModel} from './models/user-model';

@Component({
  selector: 'webapp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'client';

  user: UserModel;

  constructor() {
  }

}
