import {CellModel} from './cell-model';

export class SnakeModel {

  direction: number;
  parts: CellModel[];

  constructor(
    direction: number,
    parts: CellModel[]
  ) {
    this.direction = direction;
    this.parts = parts;
  }

}


