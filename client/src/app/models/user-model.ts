export class UserModel {

  login: string;

  password: string;
  progress: string;

  gamesnowalls: number;
  globalscorenowalls: number;

  gameswithwalls: number;
  globalscorewithwalls: number;


  constructor(
    login: string,
    password: string,
    progress: string,
    gamesnowalls: number,
    globalscorenowalls: number,
    gameswithwalls: number,
    globalscorewithwalls: number) {

    this.login = login;
    this.password = password;
    this.progress = progress;
    this.gamesnowalls = gamesnowalls;
    this.globalscorenowalls = globalscorenowalls;
    this.gameswithwalls = gameswithwalls;
    this.globalscorewithwalls = globalscorewithwalls;
  }
}
