import {SnakeModel} from './snake-model';
import {CellModel} from './cell-model';

export class ProgressModel {

  board: boolean[][];
  snake: SnakeModel;
  fruit: CellModel;
  moveInterval: number;
  gameMode: string;
  score: number;

  constructor(
    board: boolean[][],
    snake: SnakeModel,
    fruit: CellModel,
    moveInterval: number,
    gameMode: string,
    score: number) {

    this.board = board;
    this.snake = snake;
    this.fruit = fruit;
    this.moveInterval = moveInterval;
    this.gameMode = gameMode;
    this.score = score;
  }
}
