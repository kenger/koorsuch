import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpServiceService} from '../services/http-service.service';
import {Router} from '@angular/router';
import {SnackBarServiceService} from '../services/snack-bar-service.service';
import {StorageService} from '../services/storage.service';
import {UserServiceService} from '../services/user-service.service';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'webapp-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less']
})
export class SignInComponent implements OnInit {

  formGroup: FormGroup;

  isClicked: boolean;

  constructor(
    private http: HttpServiceService,
    private router: Router,
    private snackBar: SnackBarServiceService,
    private storage: StorageService,
    private userService: UserServiceService,
    private titleService: Title) {

    this.isClicked = false;

    this.formGroup = new FormGroup({
        loginF: new FormControl(''),
        passwordF: new FormControl('')
      }
    );
  }

  ngOnInit() {
    this.titleService.setTitle('Вход в систему');
  }

  getLoginErrorMessage() {
    return this.formGroup.get('loginF').hasError('pattern') ? 'Неверный логин' :
      '';
  }

  getPasswordErrorMessage() {
    return this.formGroup.get('loginF').hasError('pattern') ? 'Неверный пароль' :
      '';
  }

  submit() {
    if (!this.isClicked && this.formGroup.valid && this.formGroup.get('loginF').value && this.formGroup.get('passwordF').value) {
      this.isClicked = true;
      this.signIn();
    } else {
      this.snackBar.open('Пожалуйста, заполните приведённые поля корректно');
    }

  }

  signIn() {
    const login = this.formGroup.get('loginF').value.toString().replace(/\s/g, '');
    const password = this.formGroup.get('passwordF').value;

    this.userService.signIn(login, password).subscribe(
      () => {
        this.snackBar.open('Добро пожаловать, ' + this.userService.getLogin() + '!');
        this.router.navigateByUrl('/');
        this.isClicked = false;
      },
      error => {
        if (error.status === 401) {
          this.snackBar.open('Неверный логин или пароль!');
        } else {
          this.snackBar.open('Нет соединения с сервером. Попробуйте зайти позже');
        }
        this.isClicked = false;
      }
    );
  }

}
