import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GameComponent } from './game/game.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { TableComponent } from './table/table.component';
import { DelAccDialogComponent } from './profile/del-acc-dialog/del-acc-dialog.component';

import {SharedModule} from './shared/shared.module';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing/app-routing.module';
import { ChangePassDialogComponent } from './profile/change-pass-dialog/change-pass-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    SignUpComponent,
    SignInComponent,
    HeaderComponent,
    ProfileComponent,
    TableComponent,
    DelAccDialogComponent,
    ChangePassDialogComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    AppRoutingModule
  ],
  entryComponents: [DelAccDialogComponent, ChangePassDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
