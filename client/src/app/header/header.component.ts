import {Component, OnInit} from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {SnackBarServiceService} from '../services/snack-bar-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'webapp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  constructor(
    private userService: UserServiceService,
    private snackBar: SnackBarServiceService,
    private routing: Router) {
  }

  ngOnInit() {
  }

  getName(): string {
    return localStorage.getItem('login');
  }

  goTo(page: string) {
    if (this.userService.getAmIplaying()) {
      this.snackBar.open('Данная операция невозможна во время игры!');
    } else {
      switch (page) {
        case 'signIn':
          this.routing.navigateByUrl('/sign-in');
          break;
        case 'signOut':
          this.userService.signOut();
          break;
        case 'profile':
          this.routing.navigateByUrl('/profile');
          break;
        case 'top':
          this.routing.navigateByUrl('/table');
          break;
      }
    }
  }

}
