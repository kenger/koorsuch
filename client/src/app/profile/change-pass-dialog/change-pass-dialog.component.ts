import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MatDialogRef} from "@angular/material";
import {UserServiceService} from "../../services/user-service.service";
import {SnackBarServiceService} from "../../services/snack-bar-service.service";

@Component({
  selector: 'webapp-change-pass-dialog',
  templateUrl: './change-pass-dialog.component.html',
  styleUrls: ['./change-pass-dialog.component.less']
})
export class ChangePassDialogComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<ChangePassDialogComponent>,
    private userService: UserServiceService,
    private snackBar: SnackBarServiceService) {

    this.formGroup = new FormGroup({
        currentPassword: new FormControl(''),
        newPassword: new FormControl(''),
        repeatPassword: new FormControl('')
      }
    );
  }

  ngOnInit() {
  }

  getCurrentPasswordErrorMessage() {
    return this.formGroup.get('currentPassword').hasError('pattern') ? 'Неверный пароль' :
      '';
  }

  getNewPasswordErrorMessage() {
    return this.formGroup.get('newPassword').hasError('pattern') ? 'Новый пароль совпадает с текущим' :
      'Пароль должен содержать не менее 6-ти знаков';
  }

  getRepeatPasswordErrorMessage() {
    return this.formGroup.get('repeatPassword').hasError('pattern') ? 'Пароли не совпадают' :
      '';
  }

  checkIfNewPasswordsMatch() {
    if (
      this.formGroup.get('newPassword').value.toString().replace(/\s/g, '') !==
      this.formGroup.get('repeatPassword').value.toString().replace(/\s/g, '')
    ) {
      this.formGroup.get('repeatPassword').setErrors({pattern: true});
    }
  }

  submit() {
    this.checkIfNewPasswordsMatch();

    if (this.formGroup.valid) {
      const newPass = this.formGroup.get('newPassword').value;

      this.userService.changePassword(
        this.formGroup.get('currentPassword').value,
        newPass
      ).subscribe(
        () => {
          this.userService.updatePasswordInToken(newPass);
          this.snackBar.open('Ваш пароль был успешно сменён');
          this.dialogRef.close();
        },
        (response) => {
          if (response.error === 'samePasswords') {
            this.formGroup.get('newPassword').setErrors({pattern: true});
          } else if (response.error === 'wrongCurrentPassword') {
            this.formGroup.get('currentPassword').setErrors({pattern: true});
          } else {
            this.snackBar.open('Соединение с сервером было прервано. Попробуйте позже');
          }
        }
      );
    }

  }

}
