import {Component, OnInit} from '@angular/core';
import {UserServiceService} from '../services/user-service.service';
import {UserModel} from '../models/user-model';
import {SnackBarServiceService} from '../services/snack-bar-service.service';
import {Router} from '@angular/router';
import {MatDialog} from "@angular/material";
import {DelAccDialogComponent} from "./del-acc-dialog/del-acc-dialog.component";
import {ChangePassDialogComponent} from "./change-pass-dialog/change-pass-dialog.component";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'webapp-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

  // очки
  globalScoreNoWalls: number;
  globalScoreWithWalls: number;

  // количество партий
  gamesNoWalls: number;
  gamesWithWalls: number;

  constructor(
    private userService: UserServiceService,
    private snackBar: SnackBarServiceService,
    private router: Router,
    private dialog: MatDialog,
    private titleService: Title
  ) {

  }

  ngOnInit() {
    this.titleService.setTitle('Личный кабинет');
    if (this.userService.isSignedIn()) {
      this.getProfileInfo();
    } else {
      this.router.navigateByUrl('/');
    }
  }

  getProfileInfo() {
    this.userService.getUser().subscribe(
      (myUser: UserModel) => {
        this.globalScoreNoWalls = myUser.globalscorenowalls;
        this.globalScoreWithWalls = myUser.globalscorewithwalls;
        this.gamesNoWalls = myUser.gamesnowalls;
        this.gamesWithWalls = myUser.gameswithwalls;
      },
      () => {
        this.snackBar.open('Не удалось загрузить данные о Вашем аккаунте');
      }
    );
  }

  openChangePassDialog() {
    this.dialog.open(
      ChangePassDialogComponent,
      {
        maxWidth: '550px',
        height: 'auto',
      }
    );
  }

  openDeleteAccDialog() {
    this.dialog.open(
      DelAccDialogComponent,
      {
        maxWidth: '550px',
        height: 'auto',
      }
    );
  }

}
