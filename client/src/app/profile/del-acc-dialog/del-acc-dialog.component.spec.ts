import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelAccDialogComponent } from './del-acc-dialog.component';

describe('DelAccDialogComponent', () => {
  let component: DelAccDialogComponent;
  let fixture: ComponentFixture<DelAccDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelAccDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelAccDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
