import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {UserServiceService} from "../../services/user-service.service";
import {SnackBarServiceService} from "../../services/snack-bar-service.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'webapp-del-acc-dialog',
  templateUrl: './del-acc-dialog.component.html',
  styleUrls: ['./del-acc-dialog.component.less']
})
export class DelAccDialogComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<DelAccDialogComponent>,
    private userService: UserServiceService,
    private snackBar: SnackBarServiceService) {

    this.formGroup = new FormGroup({
        currentPassword: new FormControl('')
      }
    );
  }

  ngOnInit() {
  }

  getPasswordErrorMessage() {
    return this.formGroup.get('currentPassword').hasError('pattern') ? 'Неверный пароль' :
      '';
  }

  submit() {
    if (this.formGroup.valid) {

      this.userService.deleteAccount(this.formGroup.get('currentPassword').value).subscribe(
        () => {
          this.userService.signOut();
          this.snackBar.open('Ваш аккаунт был успешно удалён');
          this.dialogRef.close();
        },
        (response) => {
          if (response.error === 'wrongPasswords') {
            this.formGroup.get('currentPassword').setErrors({pattern: true});
          } else {
            this.snackBar.open('Соединение с сервером было прервано. Попробуйте позже');
          }
        }
      );
    }
  }

}
