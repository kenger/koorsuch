import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule, MatDialogModule,
  MatFormFieldModule,
  MatInputModule, MatListModule,
  MatMenuModule,
  MatSnackBarModule, MatTableModule, MatTabsModule
} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';

const modules = [
  CommonModule,
  ReactiveFormsModule,
  BrowserAnimationsModule,
  MatCardModule,
  BrowserModule,
  MatButtonModule,
  MatSnackBarModule,
  MatFormFieldModule,
  MatInputModule,
  MatMenuModule,
  MatListModule,
  MatTabsModule,
  MatTableModule,
  MatDialogModule,
  FormsModule
];

@NgModule({
  declarations: [],
  imports: [
    modules
  ],
  exports: [
    modules
  ]
})
export class SharedModule { }
