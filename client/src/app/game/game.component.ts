import {Component, HostListener, OnInit} from '@angular/core';
import {COLORS, CONTROLS} from './constants';
import {HttpServiceService} from '../services/http-service.service';
import {CellModel} from '../models/cell-model';
import {SnackBarServiceService} from '../services/snack-bar-service.service';
import {UserServiceService} from '../services/user-service.service';
import {SnakeModel} from '../models/snake-model';
import {ProgressModel} from '../models/progress-model';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'webapp-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less']
})
export class GameComponent implements OnInit {

  private snake;
  private fruit;
  private boardInfo;

  board = [];

  isStartClicked: boolean;
  showGameModes: boolean;
  isModeChosen: boolean;
  canMove: boolean;
  canUpdatePosition: boolean;

  boardWidth: number; // ширина квадратной доски
  private constMoveInterval: number;
  moveInterval: number; // локальная задержка перед следующим "шагом" змейки
  snakeMax: number; // максимальная длина змейки (в клетках)
  eatenFruitNumber: number; // количество съеденных фруктов
  snakeDirection: number; // вспомогательная переменная
  connectToServerAttempts: number; // количество попыток подключения к серверу

  chosenMode: string;

  constructor(
    private http: HttpServiceService,
    private snackBar: SnackBarServiceService,
    private userService: UserServiceService,
    private titleService: Title) {

    this.boardInfo = this.userService.getBoardInfo().subscribe(
      (cell: CellModel) => {
        this.boardWidth = cell.row;
        this.constMoveInterval = cell.col;
        this.moveInterval = this.constMoveInterval;
        this.snakeMax = this.boardWidth * this.boardWidth;
        this.setNewBoard();
      },
      () => {
        this.snackBar.open('Ошибка загрузки игровой формы');
      }
    );

    this.userService.getUserGameInfo();

    this.isStartClicked = false;
    this.isModeChosen = false;

    this.snake = {
      direction: CONTROLS.LEFT,
      parts: [
        {
          row: -1,
          col: -1
        }
      ]
    };

    this.fruit = {
      row: -1,
      col: -1
    };

  }

  ngOnInit() {
    this.titleService.setTitle('Змейка - играй и будь первым!');
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(e: KeyboardEvent) {
    if (e.keyCode === CONTROLS.LEFT && this.snake.direction !== CONTROLS.RIGHT) {
      this.snakeDirection = CONTROLS.LEFT;
    } else if (e.keyCode === CONTROLS.UP && this.snake.direction !== CONTROLS.DOWN) {
      this.snakeDirection = CONTROLS.UP;
    } else if (e.keyCode === CONTROLS.RIGHT && this.snake.direction !== CONTROLS.LEFT) {
      this.snakeDirection = CONTROLS.RIGHT;
    } else if (e.keyCode === CONTROLS.DOWN && this.snake.direction !== CONTROLS.UP) {
      this.snakeDirection = CONTROLS.DOWN;
    }
  }

  setNewBoard(): void {
    this.board = [];

    for (let i = 0; i < this.boardWidth; i++) {
      this.board[i] = [];
      for (let j = 0; j < this.boardWidth; j++) {
        this.board[i][j] = false;
      }
    }
  }

  setCellColor(row: number, col: number): string {
    if (this.fruit.row === row && this.fruit.col === col) {
      return COLORS.FRUIT;
    } else if (this.snake.parts[0].row === row && this.snake.parts[0].col === col) {
      return COLORS.HEAD;
    } else if (this.board[col][row]) { // если клетка доски имеет значение true - значит, в ней часть тела змейки
      return COLORS.BODY;
    }

    return COLORS.BOARD;
  }

  startButton() {
    this.isStartClicked = true; // делаем кнопку [Начать игру] неактивной
    this.showGameModes = true; // отображаем окошко с выбором режима игры
  }

  setGameMode(mode: string) {
    this.chosenMode = mode; // запомнили выбор игрового режима
    this.isModeChosen = true; // после выбора делаем кнопку неактивной

    this.showGameModes = false; // закрываем окошко с выбором режима
    this.startTheGame();
  }

  initializeGameStart() {
    this.userService.setAmIplaying(true);
    this.connectToServerAttempts = 10;
  }

  startTheGame() {
    this.initializeGameStart();

    this.eatenFruitNumber = 0;
    this.moveInterval = this.constMoveInterval;
    this.canMove = false;
    this.snakeDirection = CONTROLS.LEFT;
    this.snake = { // переинициализируем объект snake
      direction: CONTROLS.LEFT, // устанавливаем движение змейки влево
      parts: []
    };

    for (let i = 0; i < 3; i++) {
      this.snake.parts.push( // "наполняем" змейку до длины, равной трём клеткам
        {
          row: Math.floor(this.boardWidth / 2) + i,
          col: Math.floor(this.boardWidth / 2)
        }
      );
    }
    this.setNewBoard();
    this.generateFirstFruit();
  }

  continueButton() {
    this.isStartClicked = true; // делаем кнопку [Продолжить игру] неактивной
    this.initializeGameStart();
    this.getProgress();
  }

  generateRandomNumber() { // генерация случайной координаты для данной доски
    return Math.floor(Math.random() * this.boardWidth);
  }

  updateSnakePosition() {
    if (this.canMove) {
      const newHead = this.getNewSnakeHead();

      if (this.chosenMode === 'With Walls') {
        if (this.boardCollision(newHead)) {
          return this.gameOver();
        }
      } else if (this.chosenMode === 'No Walls') {
        this.wallThroughTransition(newHead);
      }

      // проверка на столкновение змейки с самой собой
      if (this.selfCollision(newHead)) {
        return this.gameOver();
      } else if (this.fruitCollision(newHead)) {
        this.eatFruit();
      }

      if (this.haveIWon()) {
        return this.victory();
      } else {
        const oldTail = this.snake.parts.pop();
        this.board[oldTail.col][oldTail.row] = false;

        this.snake.parts.unshift(newHead);
        this.board[newHead.col][newHead.row] = true;

        this.snake.direction = this.snakeDirection;
      }
    }

    if (this.canUpdatePosition) {
      setTimeout(() => {
        this.updateSnakePosition();
      }, this.moveInterval);
    } else {
      setTimeout(() => {
        this.updateSnakePosition();
      }, 1000);
    }
  }

  getNewSnakeHead() {
    const newHead = Object.assign({}, this.snake.parts[0]);

    if (this.snakeDirection === CONTROLS.LEFT) {
      newHead.col -= 1;
    } else if (this.snakeDirection === CONTROLS.RIGHT) {
      newHead.col += 1;
    } else if (this.snakeDirection === CONTROLS.UP) {
      newHead.row -= 1;
    } else if (this.snakeDirection === CONTROLS.DOWN) {
      newHead.row += 1;
    }

    return newHead;
  }

  boardCollision(part: any) { // проверка на столкновение змейки со стенкой
    return part.row === this.boardWidth || part.row === -1 || part.col === this.boardWidth || part.col === -1;
  }

  selfCollision(part: any) { // проверка на столкновение змейки с самой собой
    return this.board[part.col][part.row];
  }

  fruitCollision(part: any) { // проверка на столкновение с фруктом
    return part.row === this.fruit.row && part.col === this.fruit.col;
  }

  eatFruit() { // метод, "поедающий" фрукт, а также - генерирующий новый
    this.canMove = false; // пока фрукт не скушали - двигаться не будем

    this.http.genFruitCoordinates(this.snake.parts).subscribe(
      cell => { // если есть соединение с сервером, мы "кушаем" фрукт. Одновременно генерируем координаты для следующего фрукта
        this.connectToServerAttempts = 10;

        const row = cell.row;
        const col = cell.col;

        if (this.board[col][row]) { // если фрукт нельзя поместить в клетку [X, Y]
          return this.eatFruit(); // заного генерируем координаты фрукта
        } else {

          this.eatenFruitNumber++;
          const tail = Object.assign({}, this.snake.parts[this.snake.parts.length - 1]);

          this.snake.parts.push(tail);

          // перезапись координат фрукта
          this.fruit = {
            row,
            col
          };

          if (this.eatenFruitNumber % 5 === 0 && this.moveInterval > 30) {
            this.moveInterval -= 5;
          }

          if (this.userService.isSignedIn()) {
            this.updateProgress();
          }

          this.canMove = true;
          this.canUpdatePosition = true;
        }

      },
      () => {
        this.canUpdatePosition = false;
        if (this.connectToServerAttempts > 0) {
          this.snackBar.open('Соединение с сервером было прервано. Пробую переподключиться ' + '(' + this.connectToServerAttempts-- + ')');
          setTimeout(() => {
            this.eatFruit();
          }, 1000);
        } else {
          this.gameOver();
        }
      }
    );

  }

  generateFirstFruit() { // метод генерации фрукта, которвый вызвается один раз: только в самом начале игры
    this.http.genFruitCoordinates(this.snake.parts).subscribe(
      cell => {
        this.connectToServerAttempts = 10;

        const row = cell.row;
        const col = cell.col;

        if (this.board[col][row]) { // если фрукт нельзя поместить в клетку [X, Y]
          return this.generateFirstFruit(); // заного генерируем координаты фрукта
        } else {
          this.fruit = {
            row,
            col
          };
          this.canMove = true;
          this.canUpdatePosition = true;
          this.updateSnakePosition();
        }
      },
      () => {
        if (this.connectToServerAttempts > 0) {
          this.snackBar.open('Соединение с сервером было прервано. Пробую переподключиться ' + '(' + this.connectToServerAttempts-- + ')');
          setTimeout(() => {
            this.generateFirstFruit();
          }, 1000);
        } else {
          this.gameOver();
        }
      }
    );
  }

  wallThroughTransition(part: any) {
    if (part.row === this.boardWidth) { // пересечение нижней границы
      part.row = 0;
    } else if (part.row === -1) { // верхней
      part.row = this.boardWidth - 1;
    }

    if (part.col === this.boardWidth) { // пересечение правой границы
      part.col = 0;
    } else if (part.col === -1) { // левой
      part.col = this.boardWidth - 1;
    }
  }

  gameOver() {
    this.isStartClicked = false;
    this.isModeChosen = false;
    if (this.userService.isSignedIn()) {
      this.userService.finish(false, this.chosenMode, this.eatenFruitNumber);
    } else {
      this.snackBar.open('Вы проиграли :(');
      this.userService.setAmIplaying(false);
    }
  }

  haveIWon() {
    return this.snake.parts.length === this.snakeMax;
  }

  victory() {
    this.isStartClicked = false;
    this.isModeChosen = false;
    if (this.userService.isSignedIn()) {
      this.userService.finish(true, this.chosenMode, this.eatenFruitNumber);
    } else {
      this.snackBar.open('ПОЗДРАВЛЯЕМ!!! Вы выиграли!');
      this.userService.setAmIplaying(false);
    }
  }

  updateProgress() {
    let snakeModel: SnakeModel;
    snakeModel = new SnakeModel(this.snakeDirection, this.snake.parts);
    let fruitModel: CellModel;
    fruitModel = this.fruit;
    let progress: ProgressModel;
    progress = new ProgressModel(
      this.board,
      snakeModel,
      fruitModel,
      this.moveInterval,
      this.chosenMode,
      this.eatenFruitNumber
    );

    this.userService.updateProgress(progress).subscribe(
      () => {
        // this.snackBar.open('Прогресс сохранён');
      },
      () => {
        this.snackBar.open('Не удалось сохранить Ваш прогресс');
      }
    );
  }

  getProgress() {
    this.userService.getProgress().subscribe(
      (progress: ProgressModel) => {
        this.board = progress.board;
        this.snake = progress.snake;
        this.fruit = progress.fruit;
        this.moveInterval = progress.moveInterval;
        this.chosenMode = progress.gameMode;
        this.eatenFruitNumber = progress.score;
        this.snackBar.openAndTimeIs('Внимание...', 2500);
        setTimeout(() => {
          this.canMove = true;
          this.canUpdatePosition = true;
          this.snakeDirection = progress.snake.direction;
          this.snackBar.openAndTimeIs('Поехали!', 700);
          this.updateSnakePosition();
        }, 3000);
      },
      () => {
        this.snackBar.open('Не могу получить информацию о Вашем прогрессе');
        this.isStartClicked = false;
        this.isModeChosen = false;
      }
    );
  }

}
