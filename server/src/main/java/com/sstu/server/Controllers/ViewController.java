package com.sstu.server.Controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping({"/table", "/sign-up", "/sign-in", "/profile"})
    public String index() {
        return "forward:/index.html";
    }
}
