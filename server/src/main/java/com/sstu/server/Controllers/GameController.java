package com.sstu.server.Controllers;

import com.sstu.server.Models.CellModel;
import com.sstu.server.Services.GameService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/game")
public class GameController {

    private final GameService gameService;

    public GameController(GameService userService) {
        this.gameService = userService;
    }

    @PutMapping("/generateFruit")
    public CellModel generateFruitCoordinates(@RequestBody ArrayList<CellModel> busyCells){
        return gameService.generateFruitCoordinates(busyCells);
    }

    @GetMapping("/info")
    public CellModel getBoardAndTimeInfo() {
        return new CellModel(
                gameService.getBoardSize(),
                gameService.getTimeInterval()
        );
    }

}
