package com.sstu.server.Controllers;

import com.sstu.server.Entities.User;
import com.sstu.server.Models.ProgressModel;
import com.sstu.server.Services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("/signUp")
    public ResponseEntity createUser(@RequestBody User theUser) {
        String status = this.userService.createUser(theUser);
        if (status.equals("LoginBusy")) {
            return ResponseEntity.badRequest().body("LoginBusy");
        }
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/signIn")
    public void validateSignIn() {
        // этот метод не будет даже вызван, если пас/лог - неверные
    }

    @GetMapping("/getUser")
    public User getUserInstance(Authentication auth) {
        String login = auth.getName();
        return userService.getUser(login);
    }

    @GetMapping("/finish/{win}/{gameMode}/{fruits}")
    public Float finish(@PathVariable Boolean win, @PathVariable String gameMode, @PathVariable int fruits, Authentication auth) {
        String login = auth.getName();
        if (win) { // утраиваем кол-во набранных очков
            return userService.finish(login, gameMode, fruits * 3);
        }
        return userService.finish(login, gameMode, fruits);
    }

    @PutMapping("/updateProgress")
    public ResponseEntity updateProgress(@RequestBody ProgressModel progress, Authentication auth) {
        return userService.updateProgress(progress, auth.getName());
    }

    @GetMapping("/getProgress")
    public ProgressModel getProgress(Authentication auth) throws IOException {
        return userService.getProgress(auth.getName());
    }

    @GetMapping("/top/{withWalls}")
    public List<User> getTopPlayers(@PathVariable Boolean withWalls) {
        return userService.getTopPlayers(withWalls);
    }

    @GetMapping("/changePassword")
    public ResponseEntity changePassword(@RequestParam String currentPassword, @RequestParam String newPassword, Authentication auth) {
        return userService.changePassword(currentPassword, newPassword, auth.getName());
    }

    @DeleteMapping("/deleteAcc/{currentPassword}")
    public ResponseEntity deleteAccount(@PathVariable String currentPassword, Authentication auth){
        return userService.deleteExistingUserByLogin(currentPassword, auth.getName());
    }
}
