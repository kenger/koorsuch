package com.sstu.server.Repositories;

import com.sstu.server.Entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);

    // No Walls + Sort Desc
    Page<User> findByGlobalscorenowallsGreaterThanOrderByGlobalscorenowallsDesc(Float score, Pageable page);

    // With Walls + sort Desc
    Page<User> findByGlobalscorewithwallsGreaterThanOrderByGlobalscorewithwallsDesc(Float score, Pageable page);

    void deleteUserByLogin(String login);

}
