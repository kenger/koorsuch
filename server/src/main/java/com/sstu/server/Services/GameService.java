package com.sstu.server.Services;

import com.sstu.server.Models.CellModel;
import com.sstu.server.Repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Random;

@Service
public class GameService {

    private final UserRepository userRepository;
    final int boardSize;
    final int timeInterval;
    final ArrayList<CellModel> board;

    public GameService(UserRepository userRepository) {
        this.userRepository = userRepository;
        this.boardSize = 16; // cells
        this.timeInterval = 150; // ms
        this.board = generateBoard();
    }

    private ArrayList<CellModel> generateBoard() {
        ArrayList<CellModel> cellArray = new ArrayList<>();
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                cellArray.add(new CellModel(i, j));
            }
        }
        return cellArray;
    }

    public CellModel generateFruitCoordinates(ArrayList<CellModel> busyCells) {
        Random rnd = new Random();

        ArrayList<CellModel> freeCells = this.board;
        freeCells.removeAll(busyCells);

        int randomIndex = rnd.nextInt(freeCells.size());
        return freeCells.get(randomIndex);
    }

    public int getBoardSize() {
        return this.boardSize;
    }

    public int getTimeInterval() {
        return this.timeInterval;
    }
}
