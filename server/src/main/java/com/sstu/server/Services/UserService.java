package com.sstu.server.Services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sstu.server.Entities.User;
import com.sstu.server.Models.ProgressModel;
import com.sstu.server.Models.UserModel;
import com.sstu.server.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final float[] scoreArray = {
            (float) 0.001,
            (float) 0.01,
            (float) 1,
            (float) 5,
            (float) 10,
            (float) 30,
            (float) 60,
            (float) 90,
            (float) 150,
            (float) 300
    };

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String createUser(User theUser) {
        User userByLogin = userRepository.findByLogin(theUser.getLogin());
        if (userByLogin != null) {
            return "LoginBusy";
        }

        theUser.setPassword(passwordEncoder.encode(theUser.getPassword()));
        this.userRepository.save(theUser);
        return "OK";
    }

    public User getUser(String login) {
        return userRepository.findByLogin(login);
    }

    @Transactional
    public ResponseEntity deleteExistingUserByLogin(String curPass, String login) {
        User theUser = userRepository.findByLogin(login);

        if (!passwordEncoder.matches(curPass, theUser.getPassword())) { // если текущий пароль введен НЕверно
            return ResponseEntity.badRequest().body("wrongPasswords");
        }

        userRepository.deleteUserByLogin(login);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("There is no such a login");
        }
        return new UserModel(user);
    }

    public ResponseEntity updateProgress(ProgressModel progress, String login) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonProgress;
        try {
            jsonProgress = mapper.writeValueAsString(progress);
            User user = userRepository.findByLogin(login);
            // записываем прогресс
            user.setProgress(jsonProgress);
            userRepository.save(user);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return ResponseEntity.badRequest().body("JSON write issue");
        }
    }

    public ProgressModel getProgress(String login) throws IOException {
        String jsonProgress = userRepository.findByLogin(login).getProgress();
        if (jsonProgress == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        // после получения прогресса очищаем его
        User user = userRepository.findByLogin(login);
        user.setProgress(null);
        userRepository.save(user);

        return mapper.readValue(jsonProgress, ProgressModel.class);
    }

    public Float finish(String login, String gameMode, int fruits) {
        User user = getUser(login);

        Float localScore;
        if (fruits < 5) {
            localScore = (float) 0;
        } else {
            try {
                localScore = this.scoreArray[fruits / 10];
            } catch (ArrayIndexOutOfBoundsException exception) {
                localScore = (float) 600;
            }
        }

        Float globalScore = null;

        if (gameMode.equals("No Walls")) {
            // обновляем кол-во всех игр
            user.setGamesnowalls(user.getGamesnowalls() + 1);
            // обновляем очки
            globalScore = user.getGlobalscorenowalls() + localScore;
            user.setGlobalscorenowalls(globalScore);
        } else if (gameMode.equals("With Walls")) {
            // обновляем кол-во всех игр
            user.setGameswithwalls(user.getGameswithwalls() + 1);
            // обновляем очки
            globalScore = user.getGlobalscorewithwalls() + localScore;
            user.setGlobalscorewithwalls(globalScore);
        }
        // очищаем поле 'progress'
        user.setProgress(null);
        // "сохраняем" пользователя
        userRepository.save(user);
        // возвращаем новое обрезанное значение очков
        DecimalFormat decimalFormat = new DecimalFormat("#.###");
        return Float.parseFloat(
                decimalFormat.format(globalScore)
        );
    }

    public List<User> getTopPlayers(Boolean withWalls) {
        if (withWalls) {
            return userRepository.findByGlobalscorewithwallsGreaterThanOrderByGlobalscorewithwallsDesc(
                    (float) 0,
                    new PageRequest(0, 20)
            ).getContent();
        }
        return userRepository.findByGlobalscorenowallsGreaterThanOrderByGlobalscorenowallsDesc(
                (float) 0,
                new PageRequest(0, 20)
        ).getContent();
    }

    public ResponseEntity changePassword(String curPass, String newPass, String login) {
        User theUser = userRepository.findByLogin(login);

        if (passwordEncoder.matches(curPass, theUser.getPassword())) { // если текущий пароль введен верно

            if (passwordEncoder.matches(newPass, theUser.getPassword())) { // если старый и новый пароли совпадают
                return ResponseEntity.badRequest().body("samePasswords");
            }

            theUser.setPassword(passwordEncoder.encode(newPass));
            userRepository.save(theUser);
            return ResponseEntity.ok(HttpStatus.OK);
        }

        return ResponseEntity.badRequest().body("wrongCurrentPassword");
    }

}
