package com.sstu.server.Entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "USERS")
@Getter
@Setter
public class User {

    @Id
    private String login;

    private String password;
    @Column(columnDefinition = "TEXT")
    private String progress;

    int gamesnowalls;
    @Column(columnDefinition="numeric(12,3)")
    Float globalscorenowalls;

    int gameswithwalls;
    @Column(columnDefinition="numeric(12,3)")
    Float globalscorewithwalls;

    public User() {
    }

    public User(
            String login,
            String password,
            String progress,
            int gamesnowalls,
            Float globalscorenowalls,
            int gameswithwalls,
            Float globalscorewithwalls) {

        this.login = login;
        this.password = password;
        this.progress = progress;
        this.gamesnowalls = gamesnowalls;
        this.globalscorenowalls = globalscorenowalls;
        this.gameswithwalls = gameswithwalls;
        this.globalscorewithwalls = globalscorewithwalls;
    }
}
