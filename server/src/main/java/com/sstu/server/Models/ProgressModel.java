package com.sstu.server.Models;

public class ProgressModel {

    public boolean[][] board;
    public SnakeModel snake;
    public CellModel fruit;
    public int moveInterval;
    public String gameMode;
    public int score;

    public ProgressModel() {}
}
