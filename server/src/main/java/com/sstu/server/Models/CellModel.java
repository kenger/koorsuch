package com.sstu.server.Models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CellModel {

    public int row;
    public int col;

    public CellModel() {}

    public CellModel(
            int row,
            int col) {

        this.row = row;
        this.col = col;
    }

}
